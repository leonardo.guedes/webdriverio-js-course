const path = require('path');
const environments = require('./data/environments.js');
const ENVIRONMENT = process.env.ENVIRONMENT || 'testing';

if (!['testing', 'prod'].includes(ENVIRONMENT)) {
  console.log('Running testing by default');
}

exports.config = {
  specs: ['./tests/**/*.js'],
  exclude: [],
  maxInstances: 10,
  capabilities: [
    {
      maxInstances: 5,
      browserName: 'chrome',
      acceptInsecureCerts: true,
      'goog:chromeOptions': {
        args: ['--start-maximized'],
      },
    },
  ],

  logLevel: 'info',
  bail: 0,
  baseUrl: environments[ENVIRONMENT],
  waitforTimeout: 10000,
  connectionRetryTimeout: 120000,
  connectionRetryCount: 3,
  services: [
    [
      'image-comparison',
      {
        baselineFolder: path.join(process.cwd(), './visual-regression/baseline/'),
        formatImageName: '{tag}-{logName}-{width}x{height}',
        screenshotPath: path.join(process.cwd(), './visual-regression/'),
        savePerInstance: true,
        autoSaveBaseline: true,
        blockOutStatusBar: true,
        blockOutToolBar: true,
      },
    ],
    'selenium-standalone',
  ],

  framework: 'mocha',
  reporters: ['spec', ['allure', { outputDir: 'allure-results', disableWebdriverStepsReporting: true }]],
  mochaOpts: {
    ui: 'bdd',
    timeout: 60000,
  },

  beforeTest: function (test, context) {
    require('expect-webdriverio');
    global.wdioExpect = global.expect;

    const chai = require('chai');
    const chaiWebdriver = require('chai-webdriverio').default;
    chai.use(chaiWebdriver(browser));
    global.assert = chai.assert;
    global.expect = chai.expect;

    const { addStep } = require('@wdio/allure-reporter').default;
    global.addStep = addStep;
  },

  afterTest: async function (test, context, { error, result, duration, passed, retries }) {
    if (!passed) {
      await browser.takeScreenshot();
    }
  },
};
