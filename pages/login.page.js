import BasePage from './base.page';

class LoginPage extends BasePage {

  // WebElements
  get emailInput() {
    return $('input[type="email"]');
  }

  get passwordInput() {
    return $('input[type="password"]');
  }

  get loginButton() {
    return $('button[type="submit"][title="Login"]');
  }

  get welcomeMessage() {
    return $('.welcome-msg');
  }

  //-------------------------------------------------------------------------------------------------------//

  async enterEmail(email) {
    addStep(`Enter email: ${email}`);
    await super.clearFieldAndSendText(this.emailInput, email);
  }

  async enterPassword(password) {
    addStep(`Enter password: ${password}`);
    await super.clearFieldAndSendText(this.passwordInput, password);
  }

  async clickLoginButton() {
    addStep('Click on the Login button');
    await super.clickElement(this.loginButton);
  }

  async getWelcomeText() {
    return await this.welcomeMessage.getText();
  }
}

export default new LoginPage();
