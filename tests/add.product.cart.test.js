import DATA from '../data/products';
import homePage from '../pages/home.page';
import searchPage from '../pages/search.page';
import productPage from '../pages/product.page';
import cartPage from '../pages/cart.page';

describe('Add Product to Cart', () => {
  it('Search "glasses" and add the product to the cart', async () => {
    await homePage.open('/');
    await homePage.search('glasses');

    await searchPage.goToResult();
    await productPage.clickAddToCartButton();

    addStep('Check that the successful product added to cart message is displayed');
    expect(await cartPage.getProductAddedToCartMessage()).to.include('was added to your shopping cart');

    addStep('Check the product name on the cart includes: "glasses"');
    expect(await cartPage.getProductName()).to.include('glasses');
  });
});

