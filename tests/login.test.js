import homePage from '../pages/home.page';
import loginPage from '../pages/login.page';

describe('Login', () => {
  it(`Valid login`, async () => {
    await homePage.open('/');
    await homePage.clickAccountMenu();
    await homePage.clickLogin();

    await loginPage.enterEmail('leonardo.guedes@abstracta.com.uy');
    await loginPage.enterPassword('Ab1234567-');
    await loginPage.clickLoginButton();

    addStep('Check that the successful welcome message is displayed');
    expect(await loginPage.getWelcomeText()).to.include('WELCOME');
  });
});
