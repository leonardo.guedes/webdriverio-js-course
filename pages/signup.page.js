import BasePage from './base.page';

class SignUpPage extends BasePage {

  // WebElements
  get firstNameInput() {
    return $('#firstname');
  }

  get lastNameInput() {
    return $('#lastname');
  }

  get emailInput() {
    return $('#email_address');
  }

  get passwordInput() {
    return $('#password');
  }

  get confirmPasswordInput() {
    return $('#confirmation');
  }

  get registerButton() {
    return $('button[type="submit"][title="Register"]');
  }

  get creationSuccessMessage() {
    return $('.success-msg');
  }

  //-------------------------------------------------------------------------------------------------------//

  async generateUniqueEmail(baseEmail, domain) {
    const timestamp = Date.now();
    const uniqueEmail = `${baseEmail}+${timestamp}${domain}`;
    return uniqueEmail;
  }
  
  async enterFirstName(firstName) {
    addStep(`Enter first name: ${firstName}`);
    await super.clearFieldAndSendText(this.firstNameInput, firstName);
  }

  async enterLastName(lastName) {
    addStep(`Enter last name: ${lastName}`);
    await super.clearFieldAndSendText(this.lastNameInput, lastName);
  }

  async enterUniqueEmail(baseEmail, domain) {
    addStep(`Generate and enter unique email`);
    const uniqueEmail = await this.generateUniqueEmail(baseEmail, domain);
    await super.clearFieldAndSendText(this.emailInput, uniqueEmail);
  }

  async enterPassword(password) {
    addStep(`Enter password: ${password}`);
    await super.clearFieldAndSendText(this.passwordInput, password);
  }

  async confirmPassword(password) {
    addStep(`Confirm password: ${password}`);
    await super.clearFieldAndSendText(this.confirmPasswordInput, password);
  }

  async clickRegisterButton() {
    addStep('Click on the Register button');
    await super.clickElement(this.registerButton);
  }

  async getCreationSuccessMessage() {
    return await this.creationSuccessMessage.getText();
  }
}

export default new SignUpPage();
