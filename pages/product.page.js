import BasePage from './base.page';

class ProductPage extends BasePage {

  // WebElements
  get addToCartButton() {
    return $('.add-to-cart-buttons');
  }

  //-------------------------------------------------------------------------------------------------------//

  async clickAddToCartButton() {
    addStep('Click on the Add to Cart button');
    await super.clickElement(this.addToCartButton);
  }
}

export default new ProductPage();
