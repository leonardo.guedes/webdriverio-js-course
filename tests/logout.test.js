import homePage from '../pages/home.page';
import loginPage from '../pages/login.page';

describe('Logout', () => {
  it('Logout and verify successful logout message', async () => {
    await homePage.open('/');
    await homePage.clickAccountMenu();
    await homePage.clickLogin();

    await loginPage.enterEmail('leonardo.guedes@abstracta.com.uy');
    await loginPage.enterPassword('Ab1234567-');
    await loginPage.clickLoginButton();

    await homePage.clickAccountMenu();
    await homePage.clickLogout();

    addStep('Check that the successful logout message is displayed');
    expect(await homePage.getLogoutText()).to.include('YOU ARE NOW LOGGED OUT');
  });
});
