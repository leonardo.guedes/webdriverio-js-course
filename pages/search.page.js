import BasePage from './base.page';

class SearchPage extends BasePage {

  // WebElements
  get result() {
    return $('.product-name');
  }

  //-------------------------------------------------------------------------------------------------------//

  async goToResult() {
    addStep('Go to the search result');
    await super.clickElement(this.result);
  }

  async getResultName() {
    return (await this.result.getText()).toLowerCase();
  }
}

export default new SearchPage();
