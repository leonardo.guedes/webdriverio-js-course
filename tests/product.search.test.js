import DATA from '../data/products';
import homePage from '../pages/home.page';
import searchPage from '../pages/search.page';

describe('Search', () => {
  DATA.forEach(({ product }) => {
    it(`Search "${product}" and verify the result`, async () => {
      await homePage.open('/');
      await homePage.search(product);

      addStep(`Check the search text includes: ${product}`);
      expect(await homePage.getSearchText()).to.include(product);

      addStep(`Check the result name includes: ${product}`);
      expect(await searchPage.getResultName()).to.include(product);
    });
  });
});
