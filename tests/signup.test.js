import homePage from '../pages/home.page';
import signUpPage from '../pages/signup.page';

describe('Sign Up', () => {
  it('Sign Up and verify successful sign up message', async () => {
    await homePage.open('/');
    await homePage.clickAccountMenu();
    await homePage.clickRegister();

    await signUpPage.enterFirstName('Test');
    await signUpPage.enterLastName('User');
    await signUpPage.enterUniqueEmail('testuser', '@abstracta.com.uy');
    await signUpPage.enterPassword('Ab1234567-');
    await signUpPage.confirmPassword('Ab1234567-');
    await signUpPage.clickRegisterButton();

    addStep('Check the account creation success message: "Thank you for registering with Madison Island."');
    expect(await signUpPage.getCreationSuccessMessage()).to.equal('Thank you for registering with Madison Island.');

    addStep('Compare the account creation success message screenshot')
    expect(await browser.checkElement(await signUpPage.creationSuccessMessage, "creationSuccessMessage", {})).to.equal(0, "Error: The account creation success message does not match the expected");
  });
});
