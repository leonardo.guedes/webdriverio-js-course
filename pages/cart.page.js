import BasePage from './base.page';

class CartPage extends BasePage {

  // WebElements
  get productInfo() {
    return $('.product-cart-info');
  }

  get productAddedToCartMessage() {
    return $('.success-msg');
  }

  //-------------------------------------------------------------------------------------------------------//

  async getProductName() {
    return (await this.productInfo.getText()).toLowerCase();
  }

  async getProductAddedToCartMessage() {
    return await this.productAddedToCartMessage.getText();
  }
}

export default new CartPage();
