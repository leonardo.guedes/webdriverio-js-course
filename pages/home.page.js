import BasePage from '../pages/base.page';

class HomePage extends BasePage {

  // WebElements
  get searchInput() {
    return $('#search');
  }

  get accountMenu() {
    return $('//span[contains(@class, "label") and contains(text(), "Account")]');
  }

  get registerButton() {
    return $('a[title="Register"]');
  }

  get loginButton() {
    return $('a[title="Log In"]');
  }

  get logoutButton() {
    return $('a[title="Log Out"]');
  }

  get logoutMessage() {
    return $('.page-title');
  }

  //-------------------------------------------------------------------------------------------------------//

  async clickAccountMenu() {
    addStep('Click on the Account menu');
    await super.clickElement(this.accountMenu);
  }

  async clickRegister() {
    addStep('Click on the Register button');
    await super.clickElement(this.registerButton);
  }

  async clickLogin() {
    addStep('Click on the Login button');
    await super.clickElement(this.loginButton);
  }

  async clickLogout() {
    addStep('Click on the Logout button');
    await super.clickElement(this.logoutButton);
  }

  async getLogoutText() {
    return await this.logoutMessage.getText();
  }

  async search(product) {
    addStep(`Search article: ${product}`);
    await super.clearFieldAndSendText(this.searchInput, product);
    await this.searchInput.keys('Enter');
  }

  async getSearchText() {
    addStep('Get text from the search bar');
    return (await this.searchInput.getValue()).toLowerCase();
  }
}

export default new HomePage();