const PAGE_TIMEOUT = 10000;

export default class BasePage {
    
    /**
        * Open page
        * @param {String} route to access
        */
    async open(route) {
        await browser.url(`${route}`);
    }

    /**
        * Wait for an element to be clickable and click it
        * @param {Object} element to click
        */
    async clickElement(element) {
        await element.waitForClickable({ timeout: PAGE_TIMEOUT });
        await element.click();
    }

    /**
        * Method to send text to an element
        * @param {Object} element to receive the text
        * @param {String} text to send
        */
    async clearFieldAndSendText(element, text){
        await element.waitForClickable({ timeout: PAGE_TIMEOUT });
        await element.clearValue();
        await element.click();
        await element.keys(text);
    }
}
